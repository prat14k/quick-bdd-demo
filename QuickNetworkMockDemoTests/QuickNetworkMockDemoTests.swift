//
//  QuickNetworkMockDemoTests.swift
//  QuickNetworkMockDemoTests
//
//  Created by Prateek Sharma on 6/25/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import Quick
import Nimble
@testable import QuickNetworkMockDemo

let dict =
    [
        "data":
        [
            "attributes":
            [
                "email": "vaibhav@vinsol.com",
                "name": "Vaibhav",
                "phone": "123456789",
                "stadium_employee": 0,
                "token": "LpgBQ3Nbt99pkKWxYdPiMUXN"
            ],
            "id": 35,
            "type": "spree_runners"
        ]
    ]

struct MockRequest: RequestSenderProtocol {
    var response: Any?
    var error: RequestError?
    
    func send(responseHandler: @escaping (Any?, RequestError?) -> ()) {
        responseHandler(response, error)
    }
}

class QuickNetworkMockDemoTests: QuickSpec {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    override func spec() {
        describe("User login") {
            context("sends correct credentials", closure: {
                it("should recieve success", closure: {
                    RunnerAuthenticator.login(requestSender: MockRequest(response: dict, error: nil), parameters: [:], completionHandler: { (response, error) in
                        expect(response).toNot(beNil())
                        expect(error).to(beNil())
                    })
                })
            })
            
            context("sends incorrect credentials", closure: {
                it("should fail", closure: {
                    RunnerAuthenticator.login(requestSender: MockRequest(response: nil, error: RequestError.authorization(code: 404, msg: "Blah Blah")), parameters: [:], completionHandler: { (response, error) in
                        expect(response).to(beNil())
                        expect(error).toNot(beNil())
                    })
                })
            })
        }
    }
}
