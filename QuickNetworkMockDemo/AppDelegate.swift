//
//  AppDelegate.swift
//  QuickNetworkMockDemo
//
//  Created by Prateek Sharma on 6/24/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

