//
//  APIRequest.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 12/14/18.
//  Copyright © 2018 Kitchen Stadium. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestSenderProtocol {
    func send(responseHandler: @escaping (Any?, RequestError?) -> ())
}

struct APIRequest: URLRequestConvertible, RequestSenderProtocol {
    
    private let route: String
    private let method: HTTPMethod
    private let headers: HTTPHeaders
    private let parameters: Parameters?
    private let encoding: ParameterEncoding
    
    init(route: String, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) {
        
        self.route = route
        self.method = method
        self.parameters = parameters
        self.headers = headers ?? [:]
        self.encoding = encoding
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try route.asURL()
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = method.rawValue
        
        for (key,value) in headers {
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
        print("###API Request: \(urlRequest) \(method.rawValue)")
        print("###Headers: \(headers)")
        print("###Parametes: \(parameters ?? [:])")
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}

extension APIRequest {
    
    func send(completionHandler: ((Error?) -> ())? = nil) {
        Alamofire.request(self).responseJSON { (response: DataResponse<Any>) in
            do {
                try APIResponse.validate(response: response)
                completionHandler?(nil)
            } catch {
                print(response.result.value ?? "nil")
                completionHandler?(error)
            }
        }
    }
    
    func send(responseHandler: @escaping (Any?, RequestError?) -> ()) {
        Alamofire.request(self).responseJSON { (response: DataResponse<Any>) in
            do {
                try APIResponse.validate(response: response)
                responseHandler(response.result.value, nil)
            } catch {
                print(error)
                if error is URLError {
                    responseHandler(nil, RequestError.response(error: error.localizedDescription))
                } else if let err = error as? RequestError {
                    responseHandler(nil, err)
                } else {
                    responseHandler(nil, RequestError.unexpectedFormat(error: RequestError.commonErrorMsg))
                }
            }
        }
    }
    
}
