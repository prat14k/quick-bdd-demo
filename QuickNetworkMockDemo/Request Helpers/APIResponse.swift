//
//  APIResponse.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 1/4/19.
//  Copyright © 2019 Kitchen Stadium. All rights reserved.
//

import Foundation
import Alamofire

struct APIResponse<T> {
    
    static func validate(response: DataResponse<T>) throws {
        try checkForEmpty(responseData: response.data)
        guard response.result.error == nil  else { throw response.result.error! }
        guard let responseDict = response.result.value as? [String: Any]  else { return }
        if let error = responseDict["error"] {
            throw RequestError.response(error: (error as? String) ?? RequestError.commonErrorMsg)
        } else if let errors = responseDict["errors"] {
            let error = (errors as? [String])?.joined(separator: ",") ?? RequestError.commonErrorMsg
            throw RequestError.response(error: error)
        } 
    }
    
    static private func checkForEmpty(responseData: Data?) throws {
        guard let responseData = responseData,
            let jsonDict = try? JSONDecoder().decode([String: String].self, from: responseData),
            jsonDict.count == 0
        else { return }
        throw RequestError.dataNotFound
    }
}
