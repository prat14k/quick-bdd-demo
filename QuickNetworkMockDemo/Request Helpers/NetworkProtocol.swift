//
//  NetworkProtocol.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 12/14/18.
//  Copyright © 2018 Kitchen Stadium. All rights reserved.
//

import Foundation
import Alamofire

typealias URLParameters = [String: String]

protocol BasePathProtocol {
    static var basePath: String { get }
}

protocol RequestProtocol {
    static var route: String { get }
    static var method: HTTPMethod { get }
}
extension RequestProtocol {
    
    static func createRequest(parameters: Parameters? = nil, urlParameters: URLParameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> APIRequest {
        let apiRoute = fillParameters(params: urlParameters, inURLPath: route)
        return APIRequest(route: apiRoute, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
    
    static private func fillParameters(params: URLParameters?, inURLPath urlPath: String) -> String {
        guard let urlParameters = params  else { return urlPath }
        var apiRoute = urlPath
        for (key, value) in urlParameters {
            apiRoute = apiRoute.replacingOccurrences(of: "{{\(key)}}", with: value)
        }
        return apiRoute
    }
    
}

protocol GetRequestProtocol: RequestProtocol {}
extension GetRequestProtocol {
    static var method: HTTPMethod { return .get }
}

protocol PostRequestProtocol: RequestProtocol {}
extension PostRequestProtocol {
    static var method: HTTPMethod { return .post }
}


