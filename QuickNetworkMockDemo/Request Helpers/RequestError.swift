//
//  RequestError.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 12/14/18.
//  Copyright © 2018 Kitchen Stadium. All rights reserved.
//

import Foundation

enum RequestError: Error {
    
    case authorization(code: Int, msg: String)
    case unexpectedFormat(error: String)
    case response(error: String)
    case dataNotFound
    
    static let commonErrorMsg = "Something went wrong. Please try again later"
    static let serverErrorCode = 401
    static let notFoundCode = 404
    
}

enum SBSError: Error {
    case runtimeError(String)
}
