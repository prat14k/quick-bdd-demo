//
//  RunnerAuthenticator.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 12/14/18.
//  Copyright © 2018 Kitchen Stadium. All rights reserved.
//

import Foundation

struct RunnerAuthenticator {
    
    static private let invalidLoginError = "Invalid Pin entered. Please re-enter and try again."
    static func login(requestSender: RequestSenderProtocol? = nil,parameters: [String: Any], completionHandler:  @escaping ([String: Any]?, String?) -> Void) {
        (requestSender ?? Route.Login.createRequest(parameters: parameters)).send { (response: Any?, error: RequestError?) in
            if let dict = response as? [String: Any] {
                return completionHandler(dict, nil)
            } else {
                let error = error ?? RequestError.response(error: RequestError.commonErrorMsg)
                switch error {
                case .authorization(_, _): completionHandler(nil, invalidLoginError)
                case .unexpectedFormat(let reason), .response(let reason): completionHandler(nil, reason)
                case .dataNotFound: completionHandler(nil, RequestError.commonErrorMsg)
                }
            }
        }
    }
}
