//
//  Runner.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 12/14/18.
//  Copyright © 2018 Kitchen Stadium. All rights reserved.
//

import Foundation

class Runner: Codable {
    
    private let id: String
    private let type: String
    private let name: String?
    private let email: String
    private let phone: String
    private let stadiumEmployee: Bool
    private let token: String
    
    // API Coding Keys
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case name
        case email
        case phone
        case stadiumEmployee = "stadium_employee"
        case token
    }
    
    // Decoding
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        type = try container.decode(String.self, forKey: .type)
        
        name = try? container.decode(String.self, forKey: .name)
        email = try container.decode(String.self, forKey: .email)
        phone = try container.decode(String.self, forKey: .phone)
        stadiumEmployee = (try? container.decode(Bool.self, forKey: .stadiumEmployee)) ?? false
        token = try container.decode(String.self, forKey: .token)
    }
    
}
