//
//  Route.swift
//  Kitchen-Stadium Runner
//
//  Created by Prateek Sharma on 12/14/18.
//  Copyright © 2018 Kitchen Stadium. All rights reserved.
//

import Foundation

fileprivate enum HostRoute: String {
    case uat = "https://uat2.servedbystadium.com"
    
    var path: String {
        return "\(rawValue)/api/v1/task"
    }
    
}

struct Route: BasePathProtocol {
    
    static var basePath: String {  return HostRoute.uat.path }
   
    struct Login: PostRequestProtocol {
        static var route: String { return "\(basePath)/runners/login" }
    }
    
}
