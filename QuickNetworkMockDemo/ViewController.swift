//
//  ViewController.swift
//  QuickNetworkMockDemo
//
//  Created by Prateek Sharma on 6/24/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        RunnerAuthenticator.login(parameters: ["runner[login]": "6593"]) { (response, error) in
            guard let dict = response  else { return print(error ?? "Error") }
            print(dict)
        }
    }
}


